'use strict'

var startUrl = null;

var getStartUrl = function (cards) {
  var url = '';
  cards.forEach(function (item, i, arr) {
    url+=item.type + '%20';
  });

  var cards = {
    url: url,
    cardsTypes: cards
  };

  return cards;
};

var historyChanger = function (cards, isFirst) {
  if (history && history.pushState && location.protocol.match(/http/gi)) {
    if (isFirst) {
      history.replaceState(cards, '', cards.url);
    }

    history.pushState(cards, '', cards.url);
  }
};

var render = function (cards, isFirst) {
  var templateNode = document.querySelector('[type="text/template"]');
  var template = templateNode.innerHTML;
  var templateParent = document.getElementsByClassName('_cards-block')[0];
  var html = '';

  if (cards.length === 0) {
    templateParent.innerHTML = html;
    return false;
  }

  cards.forEach(function (item, i, arr) {
    var card = template.replace(/%type%/gi, item.type);
    html+=card;
  });

  templateParent.innerHTML = html;

  if (isFirst && location) {
    startUrl = location.pathname;
    historyChanger(getStartUrl(cards), isFirst);
  }
};

var renderClick = function (type) {
  var templateParent = document.querySelector('._cards-block');

  var card = document.createElement('li');
  card.className = 'cards-block__card ';
  card.className+='cards-block__card_' + type;
  card.setAttribute('data-type', type);

  templateParent.appendChild(card);
};

var updateState = function (state) {
  if (!state) return;

  render(state.cardsTypes);
};

var getDataForHistory = function () {
  var cardsElements = document.getElementsByClassName('cards-block__card');

  if (cardsElements.length === 0) {
    return {url: startUrl, cardsTypes: []};
  }

  var cardsTypes = [];
  var url = '';
  Array.prototype.forEach.call(cardsElements, function (item, i, arr) {
    cardsTypes.push({type: item.getAttribute('data-type')});
    url+=item.getAttribute('data-type') + '%20';
  });

  var cards = {
    url: url,
    cardsTypes: cardsTypes
  };

  return cards;
};

var init = function () {
  render(cards, true);

  document.addEventListener('click', function (e) {
    if (e.shiftKey && e.altKey) {
      renderClick('wide');
      historyChanger(getDataForHistory());
      return false;
    }

    if (e.shiftKey) {
      renderClick('narrow');
      historyChanger(getDataForHistory());
      return false;
    }

    if (e.target.className.match(/cards-block__card/gi)) {

      var cardHolder = document.getElementsByClassName('_cards-block')[0];
      if (!cardHolder.lastChild) {
        return false;
      }

      var lastCard = cardHolder.lastChild;

      lastCard.parentNode.removeChild(lastCard);
      historyChanger(getDataForHistory());
    }
  });

  window.addEventListener('popstate', function (e) {
    updateState(e.state);
  });
};

init();
